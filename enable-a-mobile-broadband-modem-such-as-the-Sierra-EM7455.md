# Intro
I have a Thinkpad T470 with a hardware mobile broadband modem: a Sierra Wireless EM7455 Qualcomm Snapdragon X7 LTE-A. It used to work perfectly fine until I updated my laptop to Ubuntu 22.04 aka 'The Windows ME of Ubuntu'. These are the steps I took to get my modem to work again. 

Before performing these steps, make sure your SIM card is enabled & works. You may test this by using another device and see if you can get a connection. If you have a connection with this SIM card you've just established that your SIM card works, so it's probably the modem causes the problem. 
  
# Problem
Mobile broadband cannot be enabled. When checking your /var/log/syslog file you see entries similar like these below after trying to use mobile broadband 

```
ModemManager[1308]: <info>  [modem0] simple connect started...
ModemManager[1308]: <info>  [modem0] simple connect state (3/8): enable
ModemManager[1308]: <info>  [modem0] state changed (disabled -> enabling)
ModemManager[1308]: <warn>  [modem0] Failure
ModemManager[1308]: <warn>  [modem0] Failure
ModemManager[1308]: <warn>  [modem0] couldn't enable interface: 'Invalid transition'
ModemManager[1308]: <info>  [modem0] state changed (enabling -> disabled)
```
Upon checking the modem using the following command in the terminal: 

`mmcli -m any`

You see something similar to this (XXX => obfuscated by me). Notice the Status header and the 'state: disabled' property 
```
          -----------------------------
          General  |              path: /org/freedesktop/ModemManager1/Modem/1
                  |         device id: XXXXXXXXXXXXXXXXXXXX
          -----------------------------
          Hardware |      manufacturer: Sierra Wireless, Incorporated
                  |             model: Sierra Wireless EM7455 Qualcomm Snapdragon X7 LTE-A
                  | firmware revision: XXXXXXXXXXXXXXXXXXX
                  |    carrier config: default
                  |      h/w revision: EM7455
                  |         supported: gsm-umts, lte
                  |           current: gsm-umts, lte
                  |      equipment id: XXXXXXXXXXXXXXXXXXX
          -----------------------------
          System   |            device: XXXXXXXXXXXXXXXXXXX
                  |           drivers: cdc_mbim
                  |            plugin: sierra
                  |      primary port: cdc-wdm2
                  |             ports: cdc-wdm2 (mbim), wwan0 (net)
          -----------------------------
          Status   |    unlock retries: sim-pin2 (3)
                  |             state: disabled
                  |       power state: low
                  |    signal quality: 0% (cached)
          -----------------------------
          Modes    |         supported: allowed: 3g; preferred: none
                  |                    allowed: 4g; preferred: none
                  |                    allowed: 3g, 4g; preferred: 4g
                  |                    allowed: 3g, 4g; preferred: 3g
                  |           current: allowed: 3g, 4g; preferred: 4g
          -----------------------------
          Bands    |         supported: utran-1, utran-3, utran-4, utran-5, utran-8, utran-2, 
                  |                    eutran-1, eutran-2, eutran-3, eutran-4, eutran-5, eutran-7, eutran-8, 
                  |                    eutran-12, eutran-13, eutran-20, eutran-25, eutran-26, eutran-41
                  |           current: utran-1, utran-3, utran-4, utran-5, utran-8, utran-2, 
                  |                    eutran-1, eutran-2, eutran-3, eutran-4, eutran-5, eutran-7, eutran-8, 
                  |                    eutran-12, eutran-13, eutran-20, eutran-25, eutran-26, eutran-41
          -----------------------------
          IP       |         supported: ipv4, ipv6, ipv4v6
          -----------------------------
          3GPP     |              imei: XXXXXXXXXXXXXXXX
                  |     enabled locks: fixed-dialing
          -----------------------------
          SIM      |  primary sim path: /org/freedesktop/ModemManager1/SIM/1
```

If you try to enable the modem using this command (it will try to enable any modem it finds) on the terminal 

`mmcli -m any -e` 

You will see once again the same message "couldn't enable interface: 'Invalid transition'" in the /var/log/syslog file

Basically the modem is disabled & cannot be enabled. 

# Solution
Apparently Modemmanager, the software taking care of your modem was changed. It does not longer automagically unluck your modem. You can read more about it here: 
https://modemmanager.org/docs/modemmanager/fcc-unlock/ According to the information on the Modemmanager site you'll need to point some files in the right direction.
Perform the following command in the terminal so the software will be allowed to enable your mobile broadband modem: 

`sudo ln -sft /etc/ModemManager/fcc-unlock.d /usr/share/ModemManager/fcc-unlock.available.d/*`

However, this is not enough on Ubuntu because it lacks a required package. There's no notice of or anything about it, I just happened to be able to find this bugreport (https://bugs.launchpad.net/ubuntu/+source/modemmanager/+bug/1968581/comments/4) which mentions that Ubuntu 22.04.1 is missing qmicli. 

You need to install libqmi-utils. Type the following into the terminal: 

`sudo apt install libqmi-utils`

After you've done this, you may or may not have to reboot. In my case I've rebooted and did a complete power-off and power-on since I didn't know what the problem was. It's very much possible that this was not needed, but just so you know. 

Now you should be able to enable your modem using the following command in the terminal: 

`mmcli -m any -e`

If this went according to plan, you should see the following message: 

"successfully enabled the modem" 

And your connection should work (again) or at least you will be able to setup a connection. Hurrah! 



 
